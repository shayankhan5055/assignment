package com.example.demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.assignment.Beans.UserProfile;
import com.assignment.db.DBManager;
import com.assignment.user.UserProcessor;

import java.util.*;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/first")
public class Controller {
	public static Logger logger = Logger.getLogger( UserServiceLogger.class.getName());	

	
	public String returnValue() {
		return "hello";
	}
	
	public static void init() {
		
		System.out.println("Init done");

		DBManager.createUsersTable();
		DBManager.createFriendsTable();
		
	}
	
	@RequestMapping("/CreateUserProfile")
	@PostMapping
	public static String createUserProfile(HttpServletRequest request)  {
		
		logger.info("Controller : createUserProfile : request " +request.toString());
		
		String result  = "User Created";
		System.out.println("request = " + request);
		
		UserProcessor.createUser(request);
		
		return result;
	}
	
	@RequestMapping("/AddFriend")
	@PostMapping
	public String addFriend(HttpServletRequest request)  {
		String result = "friend Added";
		
		UserProcessor.addFriend(request);
		//System.out.println("request = " + request);
		
		return result;
	}
	
	@RequestMapping("/ViewFriends")
	@GetMapping
	public List <UserProfile> viewFriends(HttpServletRequest request, HttpServletResponse response)  {

		List <UserProfile> friendList = UserProcessor.viewFriends(request);
		response.setContentType("application/json");

		logger.info("Controller :  viewFriends : friendList : " + friendList.toString());
		
		return friendList;
	}
	
	@RequestMapping("/ViewNearbyFriends")
	@GetMapping
	public List <UserProfile> viewNeabyFriends(HttpServletRequest request, HttpServletResponse response)  {

		List <UserProfile> friendList = UserProcessor.viewNearbyFriends(request);
		response.setContentType("application/json");

		logger.info("Controller :  viewFriends : friendList : " + friendList.toString());
		
		return friendList;
	}
	
		
	@RequestMapping("/RemoveFriend")
	@PostMapping
	public String removeFriend(HttpServletRequest request)  {
		String result = " friend removed";
		System.out.println("request = " + request);

		UserProcessor.removeFriend(request);
		return result;
	}

	
}

