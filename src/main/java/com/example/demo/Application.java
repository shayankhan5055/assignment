package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
public class Application {

	public static void main(String [] args) throws Exception {
		SpringApplication.run(Application.class, args);
		Controller.init();
	}

}
