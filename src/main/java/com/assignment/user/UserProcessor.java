package com.assignment.user;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.assignment.Beans.UserProfile;
import com.assignment.db.DBManager;
import com.example.demo.UserServiceLogger;

public class UserProcessor {
	
	public static Logger logger = Logger.getLogger( UserServiceLogger.class.getName());	

	public UserProcessor() {
		// TODO Auto-generated constructor stub
	}
	
	public static int createUser (HttpServletRequest request) {
		
		UserProfile userProfile = new UserProfile();
		
		if(request.getParameter("name") != null) {
			userProfile.setName(request.getParameter("name"));
		} if(request.getParameter("bio") != null)	{
			userProfile.setBio(request.getParameter("bio"));
		} if(request.getParameter("mobileNumber") != null) {
			userProfile.setMobileNumber(Integer.parseInt(request.getParameter("mobileNumber")));
		} if(request.getParameter("uniqueId") != null) {
			userProfile.setUniqueId(Integer.parseInt(request.getParameter("uniqueId")));
		} else { userProfile.setUniqueId(getUserUniqueId());
		
		}
		
		logger.info ("UserProcessor : createUser : userProfile : " + userProfile.toString());
		DBManager.insertUser(userProfile);
		return 0;
	}
	
	public static int addFriend (HttpServletRequest request) {
		
		int userId = 0, friendId = 0;
		
		if(request.getParameter("userUniqueId") != null)
		{
			userId = Integer.parseInt(request.getParameter("userUniqueId"));
		}
		
		if(request.getParameter("friendUniqueId") != null)
		{
			friendId = Integer.parseInt(request.getParameter("friendUniqueId"));
		}
		
		logger.info ("UserProcessor : createUser : userId : " + userId + 
						"friendId " + friendId);
		
		DBManager.inserFriend(userId, friendId);
		return 0;
	}
	
	public static int removeFriend (HttpServletRequest request) {
		
		int userId = 0, friendId = 0;
		
		if(request.getParameter("userUniqueId") != null)
		{
			userId = Integer.parseInt(request.getParameter("userUniqueId"));
		}
		
		if(request.getParameter("friendUniqueId") != null)
		{
			friendId = Integer.parseInt(request.getParameter("friendUniqueId"));
		}
		
		logger.info ("UserProcessor : createUser : userId : " + userId + 
						"friendId " + friendId);
		
		DBManager.deleteFriend(userId, friendId);
		return 0;
	}
	
	public static List<UserProfile> viewFriends (HttpServletRequest request) {
		
		int userId = 0;
		if(request.getParameter("userUniqueId") != null) {
			userId = Integer.parseInt(request.getParameter("userUniqueId"));
		}
		
		logger.info ("UserProcessor : viewFriends : userId : " + userId );
		
		 List<Integer> friendList = DBManager.getFriendList(userId);
		 List <UserProfile> friendProfileList  =  new ArrayList<UserProfile>();
		 
		 for(int friendUniqueId : friendList) {
			 
			 friendProfileList.add(DBManager.getUserProfile(friendUniqueId));
			 
		 }
		 
		return friendProfileList;
	}
	
	public static List<UserProfile> viewNearbyFriends (HttpServletRequest request) {
		
		int userId = 0, distance = 0;
		if(request.getParameter("userUniqueId") != null) {
			userId = Integer.parseInt(request.getParameter("userUniqueId"));
		} if(request.getParameter("distance") != null) {
			distance = Integer.parseInt(request.getParameter("distance"));
		}
		List <UserProfile> friendProfileList = new ArrayList<>();
		
		if(userId == 0 || distance <= 0) {
			
			logger.info ("UserProcessor : viewNearbyFriends : Params are Null, userId : " + userId + " distance " + distance );	
			
			return null;
		}
		
		logger.info ("UserProcessor : viewNearbyFriends : userId : " + userId + " distance " + distance );
		
		List<Integer> friendList = DBManager.getFriendList(userId);
		distance --;
		
		logger.info ("UserProcessor : viewNearbyFriends : friendList : " + friendList );

		if(friendList == null) { 
			return null;
		}
		
		List <Integer>friendIdListTemp = null;
		List <Integer>friendIdListTemp2 = null;

		while(distance > 0) {
			 
			 friendIdListTemp = null;
			 
			 for(int friendUniqueId : friendList) {
				 
				 friendIdListTemp = DBManager.getFriendList(friendUniqueId);
				 
				 if(friendIdListTemp != null) {
					 
					 for(int friendId : friendIdListTemp) { 
						 
						 friendIdListTemp2 = new ArrayList<>();
						 
						 if( !friendList.contains(friendId) || !friendIdListTemp2.contains(friendId) ) {
							 if(friendId != userId) { 
								 friendIdListTemp2.add(friendId);
							 }
						 }
					 }
					 
				 }
			 }
			 
			 if(friendIdListTemp2 != null) {
				 friendList.addAll(friendIdListTemp2);
			 }
			 
			logger.info ("UserProcessor : viewNearbyFriends : friendList : " + friendList );

			 
			 distance -- ;		 
		 }
		 
		 for(int friendUniqueId : friendList) {
			 
			 friendProfileList.add(DBManager.getUserProfile(friendUniqueId));
			 
		 }
		 
		return friendProfileList;
	}
	
	public static int getUserUniqueId() {
		int uniqueId = 1234;
		
		return uniqueId;
		
	}
	
	
	
}
