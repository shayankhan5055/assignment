package com.assignment.Beans;

import java.util.ArrayList;
import java.util.List;

public class UserProfile {
	
	String name;
	String bio;
	int mobileNumber;
	int uniqueId;
	List<Integer> friendUniqueIdList ;
	
	public void addFriend(int friendUniqueId) { 
		
		if(friendUniqueIdList!=null) {
			friendUniqueIdList.add(friendUniqueId);
		}
		
	}

	public UserProfile(String name, int mobileNumber, int uniqueId) {
		
		this.name = name;
		this.mobileNumber = mobileNumber;
		this.uniqueId = uniqueId;
		friendUniqueIdList = new ArrayList<Integer>();
	}
	
	@Override
	public String toString() {
		return "UserProfile [name=" + name + ", bio=" + bio + ", mobileNumber=" + mobileNumber + ", uniqueId="
				+ uniqueId + ", friendUniqueIdList=" + friendUniqueIdList + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public int getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(int mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public int getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(int uniqueId) {
		this.uniqueId = uniqueId;
	}

	public List<Integer> getFriendUniqueIdList() {
		return friendUniqueIdList;
	}

	public void setFriendUniqueIdList(List<Integer> friendUniqueIdList) {
		this.friendUniqueIdList = friendUniqueIdList;
	}

	public UserProfile() {
		
		 friendUniqueIdList = new ArrayList<Integer>();
	}

}
