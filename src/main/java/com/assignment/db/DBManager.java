package com.assignment.db;

import java.util.logging.Logger;

import java.util.*;
import com.assignment.Beans.UserProfile;
import com.example.demo.UserServiceLogger;
import java.sql.*;

public class DBManager {
	
	public static Logger logger = Logger.getLogger( UserServiceLogger.class.getName());	
	
	public static void createUsersTable() {
		
		try {
			
			Class.forName("org.h2.Driver");
			Connection h2DbConnection = DriverManager.getConnection ("jdbc:h2:~/test", "sa",""); 
			Statement stmt = h2DbConnection.createStatement(); 
			
			String sql = "CREATE TABLE   UsersTable " + 
		            "(field_name VARCHAR(255), " + 
		            " field_bio VARCHAR(255), " +  
		            " field_mobile INTEGER, " +  
		            " field_unique_id INTEGER, " +  
		            " PRIMARY KEY ( field_unique_id ))"; 
			
			stmt.execute(sql);
		} catch(Exception e) {
			
			logger.info("UsersTable : createUsersTable : Exception " + e);
			
		}
		
	}
	
	public static void createFriendsTable() {
		
		try {
			
			Class.forName("org.h2.Driver");
			Connection h2DbConnection = DriverManager.getConnection ("jdbc:h2:~/test", "sa",""); 
			Statement stmt = h2DbConnection.createStatement(); 
			
			String sql = "CREATE TABLE   FriendsTable " + 
		            "(field_user_unique_id INTEGER, " + 
		            " field_friend_unique_id INTEGER)";  
			stmt.execute(sql);
		} catch(Exception e) {
			
			logger.info("UsersTable : createFriendsTable : Exception " + e);
			
		}
		
	}
	
	public static void deleteAllFriends () {
		
		Statement stmt = null; 
		String sql = "";
		
		try {
			Class.forName("org.h2.Driver");
			Connection h2DbConnection = DriverManager.getConnection ("jdbc:h2:~/test", "sa",""); 
			stmt = h2DbConnection.createStatement(); 
	         
	        sql = "DELETE from FriendsTable";
	        logger.info("viewTablessql query  " + sql);
	        int rs = stmt.executeUpdate(sql); 
	        
	        logger.info("viewTables Result Set " + rs);
	     }
		catch (Exception e) {
				
			System.err.println(" viewTables Exception is : "+e.getMessage());	
		}
	       
	}
	
	public static void insertUser (UserProfile userProfile) {
		

		Statement stmt = null; 
		String sql = "";
		logger.info("Inserting Item ");
		try {
			
			Class.forName("org.h2.Driver");
			Connection h2DbConnection = DriverManager.getConnection ("jdbc:h2:~/test", "sa",""); 
			stmt = h2DbConnection.createStatement(); 
	        sql = "INSERT INTO UsersTable (field_name, field_bio, field_mobile, field_unique_id)" + 
			" VALUES ('" 
	        + userProfile.getName() +"', '"
			+ userProfile.getBio()+"', "
			+ userProfile.getMobileNumber() + ", "
			+ userProfile.getUniqueId()
			+")";
	         

	        logger.info ("DBManager : insertFriend : query : " + sql);
	        
	        int rs = stmt.executeUpdate(sql); 
		}
	       
		catch (Exception e) {
				
			System.err.println(" viewTables Exception is : "+e);			
		}
	       
	}
	
	public static void inserFriend (int  userId , int friendId ) {
		
		Statement stmt = null; 
		String sql = "";
		logger.info("DBManager : insertFriend : Inserting Item : userId " + userId
					+ " friendId : " + friendId);
		try {

			Class.forName("org.h2.Driver");
			Connection h2DbConnection = DriverManager.getConnection ("jdbc:h2:~/test", "sa",""); 
			stmt = h2DbConnection.createStatement(); 
			
	        sql = "INSERT INTO FriendsTable (field_user_unique_id, field_friend_unique_id)" + 
			" VALUES ('" 
	        + userId +"', '"
			+ friendId
			+"')";
	        
	        int rs = stmt.executeUpdate(sql); 
	        
	    	logger.info ("DBManager : insertFriend : query : " + sql + " result : " + rs);	
	    	
	        sql = "INSERT INTO FriendsTable (field_user_unique_id, field_friend_unique_id)" + 
	    			" VALUES ('" 
	    	        + friendId +"', '"
	    			+ userId
	    			+"')";
	        rs = stmt.executeUpdate(sql); 
	        
	    	logger.info ("DBManager : insertFriend : query : " + sql + " result : " + rs);		    	        	    		
		}
	       
		catch (Exception e) {
				
			System.err.println(" viewTables Exception is : "+e);			
		}
	       
	}
	
	public static List<Integer> getFriendList (int  userId ) {
		
		Statement stmt = null; 
		String sql = "";
		int currentItem = 1;
		List<Integer> friendList= new ArrayList<Integer>();
		
		logger.info("DBManager : getFriendList : userId " + userId);
		
		try {

			Class.forName("org.h2.Driver");
			Connection h2DbConnection = DriverManager.getConnection ("jdbc:h2:~/test", "sa",""); 
			stmt = h2DbConnection.createStatement(); 
			
	        sql = "SELECT field_friend_unique_id FROM FriendsTable WHERE field_user_unique_id = '" + 
	        + userId 
			+"'";
	        
	        ResultSet rs = stmt.executeQuery(sql); 
	        
	        while(rs.next()) { 
	        	
	        	friendList.add(rs.getInt(currentItem ++));
	        }
	        	
	    	logger.info ("DBManager : getFriendList : query : " + sql + " result : " + rs + " friendList : " + friendList);	
	     }
	       
		catch (Exception e) {
				
			System.err.println(" viewTables Exception is : "+e);			
		}
		
		return friendList;
	       
	}
	
	public static UserProfile getUserProfile (int  userId ) {
		
		Statement stmt = null; 
		String sql = "";
		int currentItem = 1;
        UserProfile userProfile = new UserProfile();
		
		logger.info("DBManager : getUserProfile : userId " + userId);
		
		try {

			Class.forName("org.h2.Driver");
			Connection h2DbConnection = DriverManager.getConnection ("jdbc:h2:~/test", "sa",""); 
			stmt = h2DbConnection.createStatement(); 
			
	        sql = "SELECT field_name, field_bio, field_mobile FROM UsersTable WHERE field_unique_id = '" + 
	        + userId 
			+"'";
	        
	        ResultSet rs = stmt.executeQuery(sql); 
	        ResultSetMetaData rsmd = rs.getMetaData();
	        
	        userProfile.setUniqueId(userId);
	        rs.next();
	        while(currentItem<4) { 
	        	
	        	logger.info("DBManager : getUserProfile : currentItem : " + currentItem);
	        	
	        	if(rsmd.getColumnName(currentItem).equalsIgnoreCase("field_name") && rs.getString(currentItem) != null) {
	        		
	        		userProfile.setName(rs.getString(currentItem));
	        	}
	        	
	        	else if(rsmd.getColumnName(currentItem).equalsIgnoreCase("field_bio") && rs.getString(currentItem) != null) {
	        		
	        		userProfile.setBio(rs.getString(currentItem));
	        	}
	        	 
	        	else if(rsmd.getColumnName(currentItem).equalsIgnoreCase("field_mobile") && rs.getInt(currentItem) > 0) {
	        		
	        		userProfile.setMobileNumber(rs.getInt(currentItem));
	        	}	        	
	        	
	        	currentItem ++;
	        }
	        	
	    	logger.info ("DBManager : getUserProfile : query : " + sql + " result : " + rs + " userProfile : " + userProfile);	
	     }
	       
		catch (Exception e) {
				
			System.err.println(" viewTables Exception is : "+e);			
		}
		
		return userProfile;
	       
	}
	
	public static void deleteFriend (int  userId , int friendId ) {
		

		Statement stmt = null; 
		String sql = "";
		logger.info("Inserting Item ");
		try {

			Class.forName("org.h2.Driver");
			Connection h2DbConnection = DriverManager.getConnection ("jdbc:h2:~/test", "sa",""); 
			stmt = h2DbConnection.createStatement(); 
	        sql = "DELETE FROM FriendsTable WHERE field_user_unique_id = '" 
	        + userId +"' AND field_friend_unique_id = '"
			+ friendId
			+"'";
	        
	        int rs = stmt.executeUpdate(sql); 
	        logger.info ("DBManager : insertFriend : query : " + sql + " result : " + rs);

	        sql = "DELETE FROM FriendsTable WHERE field_user_unique_id = '" 
	    	        + friendId +"' AND field_friend_unique_id = '"
	    			+ userId
	    			+"'";
	    	        
	    	rs = stmt.executeUpdate(sql); 
	    	logger.info ("DBManager : insertFriend : query : " + sql + " result : " + rs);	
		}
	       
		catch (Exception e) {
				
			System.err.println(" viewTables Exception is : "+e);			
		}
	       
	}
	
	public static void insertUser () {
		
		Statement stmt = null; 
		String sql = "";
		logger.info("Inserting Item ");
		try {
			
			Class.forName("org.h2.Driver");
			Connection h2DbConnection = DriverManager.getConnection ("jdbc:h2:~/test", "sa",""); 
			stmt = h2DbConnection.createStatement(); 
	        sql = "INSERT INTO UsersTable " + "VALUES (101, 202)";
	         
	        
	        int rs = stmt.executeUpdate(sql); 
		}
	       
		catch (Exception e) {
				
			System.err.println(" viewTables Exception is : "+e);			
		}
	       
	}
	
	public static void firstDB (){
		
		Statement stmt = null; 
		String sql = "";
		try {
			
			Class.forName("org.h2.Driver");
			Connection h2DbConnection = DriverManager.getConnection ("jdbc:h2:~/test", "sa",""); 
			stmt = h2DbConnection.createStatement(); 
	         sql = "INSERT INTO Registration " + "VALUES (101, 'M	ahnaz', 'Fatma', 25)";
	         
	          sql = "SELECT id, first, last, age FROM Registration"; 
	         ResultSet rs = stmt.executeQuery(sql); 
	         logger.info("Result Set " + rs.toString());
	         System.out.println(rs);
	         
	         while(true) { 
	             // Retrieve by column name 
	             int id  = rs.getInt("id"); 
	             int age = rs.getInt("age"); 
	             String first = rs.getString("first"); 
	             String last = rs.getString("last");  
	             
	             // Display values 
	             System.out.print("ID: " + id); 
	             System.out.print(", Age: " + age); 
	             System.out.print(", First: " + first); 
	             System.out.println(", Last: " + last);
	             break;
	          }
	         
			System.out.println("DB Created");
			
		} catch (Exception e) {
			
			System.err.println("Exception is : "+e);			
		}
	}
   
}
